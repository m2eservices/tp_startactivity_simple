package com.example.jeanclaude.tp_startactivity_simple;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Cet exemple est un exemple à part car les champs textes sont automatiquement sauvegardés, donc pas besoin de faire ce qu'on fait ici.
 * Cependant, cet exemple montre comment sauvegarder et restaurer l'état de données.
 */

public class    MainActivity extends AppCompatActivity {


    // chaine pour voir les modifs si on tourne l'écran
    private String chaineAmemoriser = "valeur initialisée au lancement";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // démarrage automatique de la seconde activité
//         Intent intent = new Intent(this, SecondActivity.class);
//         startActivity(intent);

        // Attention !!! chaque fois qu'on tourne l'écran, Android détruit et
        // recrée l'activité !!!
        // pour preuve, le toast ci-dessous !
        Toast.makeText(this, "création", Toast.LENGTH_SHORT).show();
    }


    public void lanceSecondActivity(View v) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);

        // Important !!!
        // on peut tuer immédiatement l'activité courante (donc impossible d'y
        // revenir ensuite)
        // Pour preuve, le Toast s'affiche lors du lancement de la seconde
        // activité

        Toast.makeText(this, "Activité 1 disparait", Toast.LENGTH_SHORT).show();
        finish();
    }

    // --------------Exemple 1 :
    // on pense sauvegarder les données pour les restaurer ensuite

//    // Exemple 1
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//
//        // quand l'activité est détruite, on peut sauver des données
//        // Exemple 1
//        // on croit sauver une chaine, mais en fait elle ne sera pas gardée au
//        // second lancement
//        chaineAmemoriser = "nouvelle valeur";
//
//        super.onSaveInstanceState(outState);
//    }
//
//    // Exemple 1
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//
//        super.onRestoreInstanceState(savedInstanceState);
//
//        // quand on relance l'activité, on peut restaurer son état (par exemple
//        // lors de la rotation de l'écran)
//        // Exemple 1
//        // on croit restaurer la valeur chaineAmemoriser qui a été mémorisée (croit-on)
//        // avec "nouvelle valeur",
//        // mais en réalité, on repart au début, et donc on affiche "valeur initiale"
//        ((TextView) findViewById(R.id.txt_monTexte)).setText(chaineAmemoriser);
//    }

    // ------------------------ Exemple 2
    // On sauve les données dans le Bundle lié à l'activité
    // Note : il y a une autre façon (pour gérer + "proprement" ce genre de
    // situation :
    // cf. http://developer.android.com/guide/topics/resources/runtime-changes.html

//	// Exemple 2
//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		// quand l'activité est détruite, on sauve les données dans le Bundle
//		// par paires clé/valeur
//		chaineAmemoriser = "sauvé";
//		outState.putString("maChaine", chaineAmemoriser);
//		// ENSUITE SEULEMENT on sauve l'état
//		super.onSaveInstanceState(outState);
//
//	}
//
//	// Exemple 2
//	@Override
//	protected void onRestoreInstanceState(Bundle savedInstanceState) {
//		super.onRestoreInstanceState(savedInstanceState);
//
//		// on récupère les valeurs sauvées
//		chaineAmemoriser = savedInstanceState.getString("maChaine");
//		((TextView) findViewById(R.id.txt_monTexte)).setText(chaineAmemoriser);
//	}
}
